<!DOCTYPE html>
<html lang="en_US">
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="format.css" />
		<title>Signing up...</title>
	</head>
	<body>

		<?php

		class Sign_up {
			/* ---------- properties ---------- */
			private $first_name;
			private $last_name;
			private $email_id;
			private $phone_no;
			private $username;
			private $pswd_Ins1;
			private $pswd_Ins2;

			/* ---------- methods ---------- */
			function __construct() {
				$this->first_name = htmlspecialchars(trim(ucfirst($_POST['fname'])));
				$this->last_name = htmlspecialchars(trim(ucfirst($_POST['lname'])));
				$this->email_id = htmlspecialchars(trim($_POST['email']));
				$this->phone_no = htmlspecialchars(trim($_POST['phone']));
				$this->username = htmlspecialchars(trim($_POST['uname']));
				$this->pswd_Ins1 = htmlspecialchars(trim($_POST['pIns1']));
				$this->pswd_Ins2 = htmlspecialchars(trim($_POST['pIns2']));
			}
				
			/* pre_action as in before writing to the file... check, check, check */
			function pre_action()
			{
				// Password (instance 1 and instance 2) compare algorithm
				if(strcmp($this->pswd_Ins1, $this->pswd_Ins2) != 0) {
					echo "The two password did not match";
					exit(0);
				}
				// Passwords match so check if it's length is minimum 8 characters
				if(strlen($this->pswd_Ins1) < 8) {
					echo "
						<div class=\"warning\">
							Password is too short! Minimum 8 characters required
						</div>
					";
					exit(0);
				}
				// Check if the new username has identical existing username
				$user_file = fopen("datas/users", "r")
							or
				die("<div class=\"warning\">Unable to open file!</div>");
				// Pre-checking file size to 0 is a good way to avoid Undefined offset
				// Notice messages at run time (due to empty file)
				//
				// This will likely happen if the user is the first user accessing
				// this site.
				$count = filesize("datas/users");
				if($count > 0) {
					$p_username = explode("\n", fread($user_file, $count));
					foreach($p_username as $index) {
						if(strcmp($this->username, $index) == 0) {
							echo "<em>" . $this->username . "</em>" .
							" was already used by someone";
							fclose($user_file);
							exit(0);
						}
					}
				}
				fclose($user_file);
				return(0);
			}
				
			/* post_action as in 'open, write & close' */
			function post_action()
			{
				$user_file = fopen("datas/users", "a")
							or
				die("<div class=\"warning\">Unable to open file!</div>");
				$password_file = fopen("datas/passwords", "a")
								or
				die("<div class=\"warning\">Unable to open file!</div>");
				// file_put_contents() is sourced from Youtube: Eli the computer guy
				// on Introduction to PHP programming
				file_put_contents("datas/users", $this->username . PHP_EOL, FILE_APPEND);
				fclose($user_file);
				file_put_contents("datas/passwords", password_hash($this->pswd_Ins1, PASSWORD_DEFAULT) . PHP_EOL, FILE_APPEND);
				fclose($password_file);
				// maintain a Comma Separated Value (aka CSV) file so spreadsheet 
				// program can open it at a later time for looking up information
				$csv_file = fopen("datas/complete-info.csv", "a")
				  				or
				die("<div class=\"warning\">Unable to open file!</div>");
				$data = $this->first_name . ", " . $this->last_name . ", " . 
					$this->email_id . ", " . $this->phone_no . ", " . 
					$this->username;

				file_put_contents("datas/complete-info.csv", $data . PHP_EOL, FILE_APPEND);
				
				fclose($csv_file);
			}
		}
		
		

		/*----------------------------------------------------------------------*/
		/*			creating an object of class Sign_up		*/
		$new_user = new Sign_up();
	  
		if($new_user->pre_action() == 0) {
			$new_user->post_action();
			print "Thank you for signing up :)";
		}

	?>
	</body>
</html>

