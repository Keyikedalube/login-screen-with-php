<!DOCTYPE html>
<html lang="en_US">
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="format.css" />
		<title>Logging in...</title>
	</head>
	<body>

		<?php

		/* Store username & password submitted to their respective variables
		 * These data are accessed using superglobal variables
		 *
		 * htmlspecialchars() prevents execution of malicious html code injection
		 * trim() removes additional whitespaces, tabs, new line, etc
		 */
		$username = htmlspecialchars(trim($_POST['name']));
		$password = htmlspecialchars(trim($_POST['password']));
		
		/* ---------- Log in algorithm ---------- */
		$user_file = fopen("datas/users", "r")
					or
		die("<div class=\"warning\">Error opening file!</div>");
		// Pre-checking file size to 0 is a good way to avoid Undefined offset
		// Notice messages at run time (due to empty file)
		//
		// This will likely happen if the user is the first user accessing this
		// site.
		$size = filesize("datas/users");
		if($size > 0) {
			// explode() function is sourced from Stackoverflow
			// on how to read file contents and store it in an array
			$username_s = explode("\n", fread($user_file, $size));
			$max_users = count($username_s);
			// this is for password index
			$count = 0;	
			foreach($username_s as $index) {
				// user is registered to this site... now check for password
				if($username == $index) {
					$password_file = fopen("datas/passwords", "r");

					$password_s = explode("\n", fread($password_file, filesize("datas/passwords")));

					// password checking algorithm
					if(password_verify($password, $password_s[$count])) {
						echo "Yay! You are in...!";
						fclose($password_file);
						fclose($user_file);
						exit(0);
					} else {
						echo "Incorrect password!";
						fclose($password_file);
						fclose($user_file);
						exit(0);
					}
				}

				// increment $password_s index
				$count++;
			}
		}

		echo "Sorry you haven't registered here yet :(";
		fclose($user_file);

		?>

	</body>
</html>

